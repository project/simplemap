<?php

// Atomic Ant Simplemap module admin
// Julian Thompson 2009
// julian@atomicant.co.uk
// www.atomicant.co.uk
// 
// Adds a new menu into blocks configuration 'Add map block' that allows the 
// addition of simple static maps with markers into a block.

function simplemap_block_list() {
  $blocks = array();
  
  $locations = simplemap_get_locations();
  
  foreach($locations as $key=>$location) {
  
    $locationtitle = (isset($location['title'])) ? $location['title'] : $key;
  
    $blocks[$key] = array(
      'info'       => t('Map : !index',array('!index'=>$locationtitle)),
      'weight'     => 0,
      'visibility' => 1,
      'cache' => BLOCK_NO_CACHE
    );
  }
  return $blocks;
}



function simplemap_block_configure($delta, $edit=array()) {
  return simplemap_map_form($delta, $edit);
}



/**
 * Alters the block admin form to add delete links next to menu blocks.
 */
function _simplemap_form_block_admin_display_form_alter(&$form, $form_state) {
  foreach (variable_get('simplemap_block_ids', array()) AS $delta) {
    $form['simplemap_' . $delta]['delete'] = array('#value' => l(t('delete'), 'admin/build/block/delete-map-block/'. $delta));
  }
}



/**
 * Deletion of menu blocks.
 */
function simplemap_delete_submit($form, &$form_state) {
  // Remove the menu block configuration variables.
  $delta = $form_state['values']['delta'];
  $block_ids = variable_get('simplemap_block_ids', array());
  unset($block_ids[array_search($delta, $block_ids)]);
  sort($block_ids);
  variable_set('simplemap_block_ids', $block_ids);
  variable_del("simplemap_block_{$delta}");

  db_query("DELETE FROM {blocks} WHERE module = 'simplemap' AND delta = %d", $delta);
  db_query("DELETE FROM {blocks_roles} WHERE module = 'simplemap' AND delta = %d", $delta);
  drupal_set_message(t('The "%name" block has been removed.', array('%name' => $form_state['values']['block_title'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/build/block';
  return;
}



function simplemap_settings() {

  // play nice with the gmap module
  if (module_exists('gmap')) {

    $form['simplemap_googleapikey'] = array(
      '#type' => 'markup',
      '#title' => t('Google Maps API Key'),
      '#value' => t('Since the GMAP module is installed Simplemap will use the Google Maps API key provided by it.  You can configure this setting under <a href="@gmapsettings">admin->settings->gmap</a>.  If the GMAP module is removed you will be able to add your api key here.  You can get a key by visiting <a href="@googlemappath">Google maps API sign up page</a>', array('@gmapsettings'=>url('admin/settings/gmap'),'@googlemappath'=>url('http://code.google.com/apis/maps/signup.html'))),
    );
    return $form;
    
  } 
  else {

    $form['simplemap_googleapikey'] = array(
      '#type' => 'textfield',
      '#title' => t('Google Maps API Key'),
      '#description' => t('Please provide a valid google api key.  You can get a key by visiting <a href="@googlemappath">Google maps API sign up page</a>', array('@googlemappath'=>url('http://code.google.com/apis/maps/signup.html'))),
      '#default_value' => variable_get('simplemap_googleapikey', ''),
    );
    return system_settings_form($form);
    
  }
}  



function simplemap_add_block_form(&$form_state) {

  $key = simplemap_get_api_key();

  if (empty($key)) {
    drupal_goto('admin/settings/simplemap');
  }
  
  module_load_include('inc','block','block.admin');
  return block_admin_configure($form_state, 'simplemap', NULL);

}



function simplemap_add_block_form_submit($form, &$form_state) {
  // Determine the delta of the new block.
  $block_ids = variable_get('simplemap_block_ids', array());
  $delta = empty($block_ids) ? 1 : max($block_ids) + 1;

  // Save the new array of blocks IDs.
  $block_ids[] = $delta;
  
  // Save the block configuration.
  simplemap_block_save($delta, $form_state['values']);
  variable_set('simplemap_block_ids', $block_ids);

  // Run the normal new block submission (borrowed from block_add_block_form_submit).
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      db_query("INSERT INTO {blocks} (visibility, pages, custom, title, module, theme, status, weight, delta, cache) VALUES(%d, '%s', %d, '%s', '%s', '%s', %d, %d, %d, %d)", $form_state['values']['visibility'], trim($form_state['values']['pages']), $form_state['values']['custom'], $form_state['values']['title'], $form_state['values']['module'], $theme->name, 0, 0, $delta, BLOCK_NO_CACHE);
    }
  }

  foreach (array_filter($form_state['values']['roles']) as $rid) {
    db_query("INSERT INTO {blocks_roles} (rid, module, delta) VALUES (%d, '%s', '%s')", $rid, $form_state['values']['module'], $delta);
  }

  drupal_set_message(t('The block has been created.'));
  cache_clear_all();

  $form_state['redirect'] = 'admin/build/block';
}



function simplemap_map_form($delta,$edit=array()) {

  if (isset($delta)) {
    $mapsettings = simplemap_get_location($delta);
  } 

  if (!$mapsettings) {
    $mapsettings = array(
      'id'=>'new',
      'title'=>'',
      'zoomlevel'=>5,
      'lat'=>0,
      'lng'=>0,
      'markers'=>array(),
    );
  }

  if (!is_array($mapsettings['allowedtypes'])) $mapsettings['allowedtypes'] = array('map','satellite','hybrid');
  
  $form = array(
    '#type' => 'markup',
    '#prefix' => '<div id="simplemap-edit">',
    '#suffix' => '</div>'  
  );
  
  $form['locationtitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Location Title'),
    '#description' => t('Type a name for this map. This is used only the block administration screen.'),
    '#default_value' => $mapsettings['title']
  );
  
  $mapid = 'simplemap'.$mapsettings['id'];
  $locationindex = 'location'.$index;
  $form[$mapid] = array(

    '#title' => 'Block '.$mapsettings['key'].' - '.$mapsettings['title'],
    '#description' => t('Define the location and details for block 1'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE
  );
  
  $form[$mapid]['map'] = array(
    '#title' => 'Map',
    '#description' => t('Define the location and details for block 1'),
    '#prefix' => '<div id="edit-'.$mapid.'-wrapper" style="margin-right:10px;" class="form-item"><label>Map:</label>',
    '#suffix' => '</div>',
    '#value' => simplemap_embedmap($mapsettings, TRUE, '', 'simplemapeditmode')
  );
  
  $form[$mapid]['centeraddress'] = array(
    '#type' => 'textfield',
    '#title' => t('Map Center Address'),
    '#description' => t('Type an address for the center of the map. This is only used to help you locate a position on the map'),
    '#default_value' => $settings['address'],
    '#attributes'=> array('class'=>'mapcenteraddress')
  );
  
  $form[$mapid]['markers'] = simplemap_markers_form($mapsettings);

  $form[$mapid]['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => 'Advanced',
    '#description' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE
  );
   
  $form[$mapid]['advanced']['centerlat'] = array(
    '#type' => 'textfield',
    '#title' => 'Map Center Latitude',
    '#default_value' => $mapsettings['mapcenterlat'],
  );
  $form[$mapid]['advanced']['centerlng'] = array(
    '#type' => 'textfield',
    '#title' => 'Map Center Longitude',
    '#default_value' => $mapsettings['mapcenterlng'],
  );
  $form[$mapid]['advanced']['allowedtypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available Map Types'),
    '#description' => t('Choose the types of maps that the viewer can access. Note you can\'t remove the currently active type'),
    '#options' => array(
      'map' => t('Normal'),
      'satellite' => t('Satellite'),
      'hybrid' => t('Hybrid')
    ),        
    '#default_value' => $mapsettings['allowedtypes'],
    '#attributes'=> array('class'=>'allowedtypes')
  );
  $form[$mapid]['advanced']['zoomlevel'] = array(
    '#type' => 'hidden',
    '#title' => 'Zoomlevel',
    '#default_value' => $mapsettings['zoomlevel']
  );
  $form[$mapid]['advanced']['type'] = array(
    '#type' => 'hidden',
    '#title' => 'MapType',
    '#default_value' => $mapsettings['type']
  );

  
  return $form;
}



function simplemap_block_save($delta, $edit) {

  if (empty($edit['delta'])) {
    $mapdataindex = 'simplemapnew';
  } 
  else {
    $mapdataindex = 'simplemap'.$delta;
  }
  
  $maptree = $edit[$mapdataindex];
  $markerindex = 'edit-'.$mapdataindex;
  $markerlist = isset($_POST[$markerindex]['markers'])?$_POST[$markerindex]['markers']:array();
  
  $markerobjlist = array();
  
  foreach($markerlist as $marker) {
    $markerobjlist[] = (object) $marker;    
  }
  
  $locationtitle = (empty($edit['locationtitle']))?('Map Block '.$delta):($edit['locationtitle']);
  
  $allowedtypes = array_keys(array_filter($maptree['advanced']['allowedtypes']));
  

  $mapsettings = array(
    'title'=>$locationtitle,
    'mapcenteraddress'=>$maptree['centeraddress'],
    'mapcenterlat'=>$maptree['advanced']['centerlat'],
    'mapcenterlng'=>$maptree['advanced']['centerlng'],
    'zoomlevel'=>$maptree['advanced']['zoomlevel'],
    'maptype'=>strtolower($maptree['advanced']['type']),
    'allowedtypes'=> $allowedtypes,
    'markers'=>$markerobjlist
    
  ); 
  
  variable_set('simplemap_block_'.$delta,$mapsettings);
}



/**
 * Menu callback: confirm deletion of map blocks.
 */
function simplemap_delete(&$form_state, $delta = 0) {
  $title = _simplemap_format_title($delta);
  $form['block_title'] = array('#type' => 'hidden', '#value' => $title);
  $form['delta'] = array('#type' => 'hidden', '#value' => $delta);

  return confirm_form($form, t('Are you sure you want to delete the "%name" block?', array('%name' => $title)), 'admin/build/block', NULL, t('Delete'), t('Cancel'));
}



function _simplemap_format_title($delta) {
  $location = simplemap_get_location($delta);
  return $location['title'];
}



function simplemap_markers_form($settings) {

  $mapid = $settings['id'];
  
  $description = t("Click the map to add markers. Markers may be updated by clicking them or their position changed by dragging the marker.  You can center the map on a marker by clicking on it's title in this list.  The id next to the marker gives the appropriate entry for theming markers in your THEME/simplemarkers.info file. If you are creating a new map you will need to save the map first and then edit to get the correct ids.");

  $form = array(
        '#type' => 'fieldset',
        '#prefix' => '<div id="edit-simplemap'.$mapid.'-markers-wrapper" class="form-item"><label>Markers:</label><div id="edit-simplemap'.$mapid.'-markers">',
        '#suffix' => '</div></div><div class="description">'.$description.'</div>',
        '#tree' => TRUE,
        '#attributes' => array('class'=>'markerlist'),
        '#theme' => 'simplemap_marker_fieldset'  
  );

  if (empty($settings['markers'])) {
    $form[] = array();
  } 
  else {
    foreach($settings['markers'] as $key=>$marker) {
    
      $markerfield = array();
    
      $markerfield['title'] = array(
        '#type' => 'hidden',
        '#value' => $marker->title
      );
      $markerfield['content'] = array(
        '#type' => 'hidden',
        '#value' => $marker->content
      );
        $markerfield['lat'] = array(
        '#type' => 'hidden',
        '#value' => $marker->lat
      );
        $markerfield['lng'] = array(
        '#type' => 'hidden',
        '#value' => $marker->lng
      );
      
      $form[$key] = $markerfield;
    }
  }

  return $form;
}



function simplemap_location_form($settings) {

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $settings['title']
  );

  $form['#prefix'] = '<div class="simplemapaddress">';
  $form['#suffix'] = '</div>';
  
  return $form;
}



function _simplemap_help($path, $arg) {

  $output = '';

  switch ($path) {
  
    case 'admin/build/block':
      $output = '<p>' . t('Use the <a href="@url">add map block page</a> to create a customizable map block. You will then be able to configure your map block before adding it.', array('@url' => url('admin/build/block/add-map-block'))) . '</p>';
      $output .= theme('more_help_link', url('admin/help/simplemap'));
    break;


    case 'admin/build/block/configure':
    case 'admin/build/block/add-map-block':
      $output = '<p>' . t('To learn more about configuring map blocks, see <a href="@url">simplemap&apos;s detailed help</a>.', array('@url' => url('admin/help/simplemap'))) . '</p>';
    break;


    case 'admin/help#simplemap':
    
      drupal_add_css(drupal_get_path('module', 'simplemap') . '/css/simplemap.admin.css'); 
      
      $output =
        '<p></p>'
      . '<h3>' . t('Configuring Simplemap') . '</h3>'
      . '<p>' . t('To use simplemap you will need a google maps api key which you can set on the <a href="@url">simplemap configuration page</a>. If you have installed the gmaps module simplemap will use this key instead and the configuration options will not be shown.', array('@url' => url('admin/settings/simplemap'))) . '</p>'
      . '<h3>' . t('Adding map blocks') . '</h3>'
      . '<p>' . t('To add new map blocks, use the "Add map block" tab (or button) on the <a href="@url">administer blocks page</a>. You will then be able to configure your map block before adding it.', array('@url' => url('admin/build/block'))) . '</p>'
      . '<h3>' . t('Configuring map blocks') . '</h3>'
      . '<p>' . t('When adding or configuring a map block, several configuration options are available:') . '</p>'
      . '<dl>'
      . '<dt><strong>' . t('Block title') . '</strong></dt>'
      . '<dd>' . t('The title of this block as displayed to the viewer. Use &lt;none&gt; to hide the title.  If unset the Location title will be used.').'</p></dd>'
      . '<dt><strong>' . t('Location title') . '</strong></dt>'
      . '<dd>' . t('The location title is used in the block administration screen and as the blocks title if Block Title is unset.') . '</dd>'
      . '<dt><strong>' . t('Map') . '</strong></dt>'
      . '<dd>' . t('The map center and zoom level will be pretty much what you get in your end map depending on the sizes configured in your style sheets.  Drag the map to move around. Click to add a marker.  Double click to zoom.  Markers can be configured/deleted by clicking directly on a marker.') . '</dd>'
      . '<dt><strong>' . t('Map Center Address') . '</strong></dt>'
      . '<dd>' . t('Type an address for the center of the map. This is only used to help you locate a position on the map.  Just type an address and the map will be updated automatically.') . '</dd>'
      . '<dt><strong>' . t('Markers') . '</strong></dt>'
      . '<dd>' . t('Markers may be updated by clicking them or their position changed by dragging the marker.  A marker can be deleted by clicking on the delete or edit links.  If no details are specified the marker will not have an info window.  If only one marker is added then this marker will be open by default.') . '</dd>'
      . '<dt><strong>' . t('Map Center Latitude (Advanced)') . '</strong></dt>'
      . '<dd>' . t('The map center latitude can be set or viewed here.  Just edit the value and the map will update automatically.') . '</dd>'
      . '<dt><strong>' . t('Map Center Longitude (Advanced)') . '</strong></dt>'
      . '<dd>' . t('The map center latitude can be set or viewed here.  Just edit the value and the map will update automatically.') . '</dd>'
      . '<dt><strong>' . t('Available Map Types (Advanced)') . '</strong></dt>'
      . '<dd>' . t('Choose the types of maps that the viewer can access. Note you can\'t remove the currently active type.') . '</dd>'
      . '</dl>'
      . '<h3>' . t('Styling map blocks') . '</h3>'
      . '<p>' . t('Themers can use the simplemapid and mapcanvas class to size and scale the map and change the border settings.') . '</p>'
      . '<dl>'
      . '<dt>' . theme('placeholder', 'Info window') . '</dt>'
      . '<dd>' . t('A %div wrapped around the info window title and content allows styling of info window content.  The class of all info windows is "markercontent".  Titles have the class "locationtitle" and content "locationcontent".  Individual cases per map can be styled using appropriately nested styles. An individual info window can be accessed using the id "MAPID-marker-MARKERINDEX"', array('%div' => '<div>')) . '</dd>'
      . '</dl>'
      . '<dl>'
      . '<dt>' . theme('placeholder', 'Markers') . '</dt>'
      . '<dd>'
      . '<p>' . t('Markers can be styled by creating a "simplemap.markers" file within the active theme folder.  This can contain sections \'default\', \'MAPID-default\' and \'MAPID-markerID\'.  You can see a list of the marker ids within the map block edit page.  Please save the map before referring to id\'s as markers may be reindexed on save. An EXAMPLE configuration could look like :') . '</p>'
      . '<p class="simplemapcodeblock">[default]<br/>image-path=images/simplemap1-marker.png<br/><span class="optional">image-width=67</span><br/><span class="optional">image-height=99</span><br/><span class="optional">shadow-path=images/simplemap1-markershadow.png</span><br/><span class="optional">shadow-width=101</span><br/><span class="optional">shadow-height=99</span><br/><span class="optional">iconanchor-x=32</span><br/><span class="optional">iconanchor-y=99</span><br/><span class="optional">infoanchor-x=5</span><br/><span class="optional">infoanchor-y=1</span><br/><br/>[simplemap1-default]<br/>image-path=images/simplemap1-car.png<br/><span class="optional">image-width=67</span><br/><span class="optional">image-height=99</span><br/><span class="optional">shadow-path=images/simplemap1-carshadow.png</span><br/><span class="optional">shadow-width=101</span><br/><span class="optional">shadow-height=99</span><br/><span class="optional">iconanchor-x=32</span><br/><span class="optional">iconanchor-y=99</span><br/><span class="optional">infoanchor-x=5</span><br/><span class="optional">infoanchor-y=1</span><br/><br/>[simplemap1-marker2]<br/>image-path=images/simplemap1-bus.png<br/><span class="optional">image-width=67</span><br/><span class="optional">image-height=99</span><br/><span class="optional">shadow-path=images/simplemap1-busshadow.png</span><br/><span class="optional">shadow-width=101</span><br/><span class="optional">shadow-height=99</span><br/><span class="optional">iconanchor-x=32</span><br/><span class="optional">iconanchor-y=99</span><br/><span class="optional">infoanchor-x=5</span><br/><span class="optional">infoanchor-y=1</span><br/></p>'
      . '<p>Grey values are optional but recommended.  Image sizes will be calculated if not provided - which will add a tiny overhead. An example simplemap.markers is included in the simplemap module under \'example files\'</p>'
      . '<h4>image-path</h4>'
      . '<p>The path to the marker image relative to the theme</p>'
      . '<h4>image-width <em>and</em> image-height</h4>'
      . '<p>The widths and heights of the marker image respectfully.</p>' 
      . '<h4>shadow-path</h4>'
      . '<p>The path to the marker shadow image relative to the theme</p>'
      . '<h4>shadow-width <em>and</em> shadow-height</h4>'
      . '<p>The widths and heights of the marker shadow image respectfully.</p>'  
      . '<h4>iconanchor-x <em>and</em> iconanchor-y</h4>'
      . '<p>Where the anchor point of the marker is. Relative to the top left corner of the image.</p>'      
      . '<h4>infoanchor-x <em>and</em> infoanchor-y</h4>'
      . '<p>Where the info window should be anchored to the marker. Relative to the top left corner of the image.</p>'
      . '</dd>'
      . '</dl>'
      . '<p>Simplemap was written by Julian Thompson from <a href="http://www.atomicant.co.uk" target="_blank">Atomic Ant Limited</a></p>';
    break;
  }
  return $output;
}

